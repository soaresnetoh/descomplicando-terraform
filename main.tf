provider "aws" {
  region  = "us-east-1"
  version = "~> 3.0"
}

provider "aws" {
  alias = "west"
  region  = "us-east-1"
  version = "~> 3.0"
}

terraform {
  backend "s3" {
    # Lembre de trocar o bucket para o seu, não pode ser o mesmo nome
    bucket = "descomplicando-terraform-hernani"
    dynamodb_table = "terraform-state-lock-dynamo"
    key    = "terraform-test.tfstate"
    region = "us-east-1"
    encrypt = true
  }
  
  # required_providers {
  #   mycloud = {
  #     source  = "hashicorp/aws"
  #     version = "~> 3.0"
  #   }
  # }
}