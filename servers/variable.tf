variable "image_id" {
    default = "ami-123456"
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
  validation {
    condition     = length(var.image_id) > 4 && substr(var.image_id, 0, 4) == "ami-"
    error_message = "The image_id value must be a valid AMI id, starting with \"ami-\"."
  }
}

variable "hash_commit" {
  default = "806d52dafe9b7fddbc4f0d2d41086ed3"
}

variable "servers" {
  
}